package com.example.fortknock.moviefinder.global;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by fortknock on 7/18/16.
 */
public class MovieApplication extends Application {
    @Override
    public void onCreate(){
        super.onCreate();
        LeakCanary.install(this);
    }
}
