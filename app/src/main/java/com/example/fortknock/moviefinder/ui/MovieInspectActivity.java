package com.example.fortknock.moviefinder.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fortknock.moviefinder.R;
import com.example.fortknock.moviefinder.misc.CustomTypefaceSpan;
import com.example.fortknock.moviefinder.misc.Utils;
import com.example.fortknock.moviefinder.pojos.Movie;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fortknock on 7/9/16.
 */
public class MovieInspectActivity extends AppCompatActivity {

    @BindView(R.id.inspect_title)
    TextView title;
    @BindView(R.id.inspect_tye)
    TextView type;
    @BindView(R.id.inspect_rating)
    TextView rating;
    @BindView(R.id.inspect_plot)
    TextView plot;
    @BindView(R.id.inspect_poster)
    ImageView poster;
    @BindView(R.id.inspect_director)
    TextView inspectDirector;
    @BindView(R.id.inspect_actors)
    TextView inspectActors;
    @BindView(R.id.inspect_writers)
    TextView inspectWriters;
    @BindView(R.id.inspect_runtime)
    TextView inspectRuntime;
    @BindView(R.id.inspect_duration)
    TextView inspectDuration;
    @BindView(R.id.inspect_country)
    TextView inspectCountry;


    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.movie_inspect);
       // Utils.hideActionBar(this);
        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();
        Movie m = (Movie) bundle.getParcelable("movie");

        styleTextView(title, m.getTitle(), " (" + m.getYear() + ")");

        if (m.getType() != null)
            type.setText(m.getType().toString() + " - " + m.getGenre());
        if (m.getYear() != null)
            //     year.setText(" (" + m.getYear().toString() + ")");
            if (m.getImdbRating() != null)
                rating.setText(m.getImdbRating() + "/10");

        styleTextView(plot, "Plot: ", m.getPlot());
        styleTextView(inspectDirector, "Director: ", m.getDirector());
        styleTextView(inspectWriters, "Writers: ", m.getWriter());
        styleTextView(inspectActors, "Actors: ", m.getActors());

        inspectDuration.setText(m.getReleased());
        inspectCountry.setText(m.getCountry());
        inspectRuntime.setText(m.getRuntime());

//        inspectDirector.setText("Director: " +m.getDirector());
        //      inspectActors.setText("Actors: " +m.getActors());
        //     inspectWriters.setText("Writer: " +m.getWriter());


        String url = m.getPoster();
        if (URLUtil.isValidUrl(url)) {
            Picasso.with(getApplicationContext()).load(url).fit().placeholder(R.drawable.progress_animation).error(R.drawable.ic_error_black_24dp).into(poster);
        }
    }


    private void styleTextView(TextView view, String type, String information) {
        if (information == null || information.isEmpty()) {
            view.setVisibility(View.INVISIBLE);
        } else {
            Spannable spannable = new SpannableString(type + information);
            spannable.setSpan(new CustomTypefaceSpan("sans-serif", Typeface.create("sans-serif", Typeface.BOLD)), 0, type.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new CustomTypefaceSpan("sans-serif", Typeface.DEFAULT), type.length(), type.length() + information.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(spannable);
        }
    }
}
