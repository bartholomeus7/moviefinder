package com.example.fortknock.moviefinder.misc;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by fortknock on 7/11/16.
 */
public class Utils {

    private Utils(){
        throw new RuntimeException("Static utility class");
    }

    public static void hideActionBar(AppCompatActivity a){
        View decorView = a.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        ActionBar actionBar = a.getSupportActionBar();
        actionBar.hide();
    }

}
