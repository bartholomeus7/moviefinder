package com.example.fortknock.moviefinder.pojos;

import com.google.gson.annotations.SerializedName;

public enum PlotType{
        @SerializedName("short") Short, @SerializedName("full") Full;
    }