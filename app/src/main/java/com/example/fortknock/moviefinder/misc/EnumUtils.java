package com.example.fortknock.moviefinder.misc;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;

public class EnumUtils {
    static public <E extends Enum<E>> String GetSerializedNameValue(E e) {
        String value = null;
        try {
            value = e.getClass().getField(e.name()).getAnnotation(SerializedName.class).value();
        } catch (NoSuchFieldException exception) {
            exception.printStackTrace();
        }
        return value;
    }

    @Retention(RetentionPolicy.RUNTIME)
    public static @interface StringRessourceBound{
        int resId();
    }

    public static <T extends Enum<T>> T parseEnumFromString(Context context, String string, Class<T> type){
        Field[] fields = type.getDeclaredFields();
        for (Field f : fields) {
            Annotation[] annos = f.getDeclaredAnnotations();
            for (Annotation a : annos) {
                if (a instanceof StringRessourceBound) {
                    int ressource = ((StringRessourceBound) a).resId();
                    String resString = context.getString(ressource);
                    if (resString.equals(string)) {
                        f.setAccessible(true);
                        try {
                            return (T) f.get(null);
                        } catch (Exception e) {//ignore}
                        }
                    }
                }
            }
        }
        return null;
    }



}