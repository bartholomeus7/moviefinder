package com.example.fortknock.moviefinder.pojos;

import com.example.fortknock.moviefinder.R;
import com.example.fortknock.moviefinder.misc.EnumUtils;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public enum MovieType implements Serializable {
        @SerializedName("movie")
        @EnumUtils.StringRessourceBound(resId=R.string.movie_type_movie)
        Movie,@SerializedName("series")
        @EnumUtils.StringRessourceBound(resId=R.string.movie_type_series)
        Series,
        @SerializedName("episode")
        @EnumUtils.StringRessourceBound(resId=R.string.movie_type_episode)
        Episode;
    }