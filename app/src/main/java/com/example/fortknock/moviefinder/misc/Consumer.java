package com.example.fortknock.moviefinder.misc;

/**
 * Created by fortknock on 7/11/16.
 */
public interface Consumer<X> {
    public void consume(X x );
}
