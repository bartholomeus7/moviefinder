package com.example.fortknock.moviefinder.pojos;



import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Movie implements Parcelable {

@SerializedName("Title")
@Expose
private String title;
@SerializedName("Year")
@Expose
private String year;
@SerializedName("Rated")
@Expose
private String rated;
@SerializedName("Released")
@Expose
private String released;
@SerializedName("Runtime")
@Expose
private String runtime;
@SerializedName("Genre")
@Expose
private String genre;
@SerializedName("Director")
@Expose
private String director;
@SerializedName("Writer")
@Expose
private String writer;
@SerializedName("Actors")
@Expose
private String actors;
@SerializedName("Plot")
@Expose
private String plot;
@SerializedName("Language")
@Expose
private String language;
@SerializedName("Country")
@Expose
private String country;
@SerializedName("Awards")
@Expose
private String awards;
@SerializedName("Poster")
@Expose
private String poster;
@SerializedName("Metascore")
@Expose
private String metascore;
@SerializedName("imdbRating")
@Expose
private String imdbRating;
@SerializedName("imdbVotes")
@Expose
private String imdbVotes;
@SerializedName("imdbID")
@Expose
private String imdbID;
@SerializedName("Type")
@Expose
private MovieType type;
@SerializedName("Response")
@Expose
private String response;

/**
*
* @return
* The title
*/
public String getTitle() {
return title;
}



/**
*
* @return
* The year
*/
public String getYear() {
return year;
}


/**
*
* @return
* The rated
*/
public String getRated() {
return rated;
}


/**
*
* @return
* The released
*/
public String getReleased() {
return released;
}


/**
*
* @return
* The runtime
*/
public String getRuntime() {
return runtime;
}


/**
*
* @return
* The genre
*/
public String getGenre() {
return genre;
}


/**
*
* @return
* The director
*/
public String getDirector() {
return director;
}

/**
*
* @return
* The writer
*/
public String getWriter() {
return writer;
}

/**
*
* @return
* The actors
*/
public String getActors() {
return actors;
}

/**
*
* @return
* The plot
*/
public String getPlot() {
return plot;
}

/**
*
* @return
* The language
*/
public String getLanguage() {
return language;
}


/**
*
* @return
* The country
*/
public String getCountry() {
return country;
}


/**
*
* @return
* The awards
*/
public String getAwards() {
return awards;
}



/**
*
* @return
* The poster
*/
public String getPoster() {
return poster;
}


/**
*
* @return
* The metascore
*/
public String getMetascore() {
return metascore;
}


/**
*
* @return
* The imdbRating
*/
public String getImdbRating() {
return imdbRating;
}


/**
*
* @return
* The imdbVotes
*/
public String getImdbVotes() {
return imdbVotes;
}



/**
*
* @return
* The imdbID
*/
public String getImdbID() {
return imdbID;
}



/**
*
* @return
* The type
*/
public MovieType getType() {
return type;
}

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", rated='" + rated + '\'' +
                ", released='" + released + '\'' +
                ", runtime='" + runtime + '\'' +
                ", genre='" + genre + '\'' +
                ", director='" + director + '\'' +
                ", writer='" + writer + '\'' +
                ", actors='" + actors + '\'' +
                ", plot='" + plot + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", awards='" + awards + '\'' +
                ", poster='" + poster + '\'' +
                ", metascore='" + metascore + '\'' +
                ", imdbRating='" + imdbRating + '\'' +
                ", imdbVotes='" + imdbVotes + '\'' +
                ", imdbID='" + imdbID + '\'' +
                ", type='" + type + '\'' +
                ", response='" + response + '\'' +
                '}';
    }


/**
*
* @return
* The response
*/
public String getResponse() {
return response;
}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.year);
        dest.writeString(this.rated);
        dest.writeString(this.released);
        dest.writeString(this.runtime);
        dest.writeString(this.genre);
        dest.writeString(this.director);
        dest.writeString(this.writer);
        dest.writeString(this.actors);
        dest.writeString(this.plot);
        dest.writeString(this.language);
        dest.writeString(this.country);
        dest.writeString(this.awards);
        dest.writeString(this.poster);
        dest.writeString(this.metascore);
        dest.writeString(this.imdbRating);
        dest.writeString(this.imdbVotes);
        dest.writeString(this.imdbID);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.response);
    }

    public Movie() {
    }

    protected Movie(Parcel in) {
        this.title = in.readString();
        this.year = in.readString();
        this.rated = in.readString();
        this.released = in.readString();
        this.runtime = in.readString();
        this.genre = in.readString();
        this.director = in.readString();
        this.writer = in.readString();
        this.actors = in.readString();
        this.plot = in.readString();
        this.language = in.readString();
        this.country = in.readString();
        this.awards = in.readString();
        this.poster = in.readString();
        this.metascore = in.readString();
        this.imdbRating = in.readString();
        this.imdbVotes = in.readString();
        this.imdbID = in.readString();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : MovieType.values()[tmpType];
        this.response = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
