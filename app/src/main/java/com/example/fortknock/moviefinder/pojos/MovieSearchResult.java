package com.example.fortknock.moviefinder.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fortknock on 7/9/16.
 */
public class MovieSearchResult {
    @SerializedName("Search")
    @Expose
    private List<Movie> search;
    @SerializedName("totalResults")
    @Expose
    private int totalResults;
    @SerializedName("Response")
    @Expose
    private boolean response;
    @SerializedName("Error")
    @Expose
    private String error;



    public int getTotalResults() {
        return totalResults;
    }


    public boolean isResponse() {
        return response;
    }

    public String getError(){return  error;}

    public List<Movie> getSearch() {
        return search;
    }

    @Override
    public String toString() {
        return "MovieSearchResult{" +
                "search=" + search +
                ", totalResults=" + totalResults +
                ", response=" + response +
                ", error='" + error + '\'' +
                '}';
    }
}
