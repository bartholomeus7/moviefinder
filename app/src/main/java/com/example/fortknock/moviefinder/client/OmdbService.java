package com.example.fortknock.moviefinder.client;

import com.example.fortknock.moviefinder.pojos.Movie;
import com.example.fortknock.moviefinder.pojos.MovieSearchResult;
import com.example.fortknock.moviefinder.pojos.MovieType;
import com.example.fortknock.moviefinder.pojos.PlotType;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by fortknock on 7/9/16.
 */
public interface OmdbService {

    final String ENDPOINT = "http://www.omdbapi.com";

    @GET("/")
    Call<Movie> getMovie(@Query("t") String title, @Query("i") String imdbId, @Query("plot") PlotType plotType);

    @GET("/")
    Call<MovieSearchResult> searchMovies(@Query("s") String titleSearch, @Query("type") MovieType type, @Query("y") Integer yearOfRelease, @Query("page") Integer pages);

}
