package com.example.fortknock.moviefinder.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fortknock.moviefinder.R;
import com.example.fortknock.moviefinder.pojos.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieListAdapter extends ArrayAdapter<Movie> {
  private final Context context;
  private final List<Movie> movies;


  public MovieListAdapter(Context context, final int ressource, List<Movie> objects) {
    super(context, -1, objects);
    this.context = context;
    this.movies = objects;

  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.movie_list_item, parent, false);
    Movie movie = movies.get(position);
    TextView movieTitle = (TextView) rowView.findViewById(R.id.firstLine);
    TextView movieType = (TextView) rowView.findViewById(R.id.secondLine);
   // TextView index = (TextView) rowView.findViewById(R.id.index_position);
    movieTitle.setText(movie.getTitle());
   // index.setText(String.valueOf(1+position));

    StringBuilder builder = new StringBuilder();
    if (movie.getType()!=null) builder.append(movie.getType().toString()).append(" (");
    if (movie.getYear()!=null) builder.append(movie.getYear()).append(")");

    if (movie.getType()!=null){
      movieType.setText(builder.toString());
    }

    final ImageView view = (ImageView) rowView.findViewById(R.id.icon);
    if (movie.getPoster()!=null && URLUtil.isValidUrl(movie.getPoster())){
      Picasso.with(getContext()).load(movie.getPoster()).fit().centerCrop().placeholder(R.drawable.progress_animation).error(R.drawable.ic_error_black_24dp).into(view);
    }

    return rowView;
  }
} 

