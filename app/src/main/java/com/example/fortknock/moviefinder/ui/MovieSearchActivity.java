package com.example.fortknock.moviefinder.ui;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fortknock.moviefinder.R;
import com.example.fortknock.moviefinder.client.MovieByIdService;
import com.example.fortknock.moviefinder.client.MovieBySearchService;
import com.example.fortknock.moviefinder.client.OmdbServiceGenerator;
import com.example.fortknock.moviefinder.misc.EnumUtils;
import com.example.fortknock.moviefinder.misc.Utils;
import com.example.fortknock.moviefinder.pojos.Movie;
import com.example.fortknock.moviefinder.pojos.MovieSearchResult;
import com.example.fortknock.moviefinder.pojos.MovieType;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnItemClick;
import icepick.Icepick;
import icepick.State;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieSearchActivity extends AppCompatActivity implements AbsListView.OnScrollListener, AdapterView.OnItemSelectedListener {


    /**
     * Bindings for ui widgets
     */


    @BindView(R.id.finder_result)
    TextView finderResult;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.finder_loading_view)
    ImageView finderLoadingView;
    @BindView(R.id.search_type)
    Spinner searchType;
    @BindView(R.id.movie_list_view)
    ListView movieListView;
    @BindView(R.id.year_chooser)
    EditText yearChooser;

    /**
     * Services
     */

    private MovieByIdService movie_by_id;
    private MovieBySearchService movie_by_search;

    /**
     * Adapters
     */

    MovieListAdapter movie_adapter;


    /**
     * States
     */

    @State
    ArrayList<Movie> movies = new ArrayList<>();
    @State
    int current_page = 1;
    @State
    int max_search_results = 0;
    @State
    volatile SearchState state = SearchState.Not_started;

    //flag which indicates if the activity is currently loading movies
    AtomicBoolean flag_loading = new AtomicBoolean(false);

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        searchAndUpdateList(false);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) { }


    enum SearchState {
        Not_started, Active_Search, Search_Success, Search_Error;
    }

    private void initServices() {
        movie_by_id = OmdbServiceGenerator.createService(MovieByIdService.class);
        movie_by_search = OmdbServiceGenerator.createService(MovieBySearchService.class);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setContentView(R.layout.movie_search);
        Utils.hideActionBar(this);

        ButterKnife.bind(this);

        initServices();
        updateResultText();
        updateSearchState();
        initSearchwindow();


        searchView.setIconified(false);
        searchView.requestFocus();

        movie_adapter = new MovieListAdapter(this, R.layout.movie_list_item, movies);
        movieListView.setAdapter(movie_adapter);
        movieListView.setOnScrollListener(this);
        searchType.setOnItemSelectedListener(this);
        yearChooser.setOnEditorActionListener((v,i,k)->{
                if (i== EditorInfo.IME_ACTION_DONE){
                    searchAndUpdateList(false);
                    return true;
                }
            return false;
        });
    }

    @OnFocusChange(R.id.year_chooser)
    public void onYearEntered(View view, boolean hasFocus){
        if (!hasFocus) searchAndUpdateList(false);
    }

    private void initSearchwindow() {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
    }

    private Integer parseYear(){
        try{
            return Integer.parseInt(yearChooser.getText().toString());
        } catch(Exception e){
            return null;
        }
    }

    private void searchAndUpdateList(final boolean expansionSearch) {
        //we do not need to searchAndUpdateList further if all searchAndUpdateList elements are already displayed
        if (expansionSearch && movies.size() >= max_search_results || searchView.getQuery().toString().isEmpty()) return;
        if (!flag_loading.compareAndSet(false, true)) return;
        if (!expansionSearch) {
            //this a new searchAndUpdateList, so delete old searchAndUpdateList results
            this.movies.clear();
            movie_adapter.notifyDataSetChanged();
        }

        String search= searchView.getQuery().toString();
        MovieType type = EnumUtils.parseEnumFromString(getApplicationContext(), searchType.getSelectedItem().toString(), MovieType.class);
        Integer year = parseYear();
        int page = expansionSearch ? ++current_page : (current_page = 1);

        final Call<MovieSearchResult> movies = movie_by_search.searchMovies(search, type, year, page);

        setState(SearchState.Active_Search);
        updateSearchState();

        movies.enqueue(new Callback<MovieSearchResult>() {
            @Override
            public void onResponse(Call<MovieSearchResult> call, Response<MovieSearchResult> response) {
                final MovieSearchResult searchResult = response.body();
                if (searchResult != null) {
                    setState(SearchState.Search_Success);
                    if (!searchResult.isResponse() && searchResult.getError()!=null){
                        printErrorMessage(searchResult.getError());
                    }

                    if (searchResult.getSearch() != null) {
                        MovieSearchActivity.this.movies.addAll(response.body().getSearch());
                        movie_adapter.notifyDataSetChanged();
                    }
                    MovieSearchActivity.this.max_search_results = searchResult.getTotalResults();
                    if (!expansionSearch) {
                        runOnUiThread(MovieSearchActivity.this::updateResultText);
                    }
                    runOnUiThread(MovieSearchActivity.this::updateSearchState);
                }

                flag_loading.set(false);

            }

            @Override
            public void onFailure(Call<MovieSearchResult> call, Throwable t) {
                t.printStackTrace();
                setState(SearchState.Search_Error);
                flag_loading.set(false);
            }
        });
    }

    private void printErrorMessage(String error){
        runOnUiThread(()->Toast.makeText(MovieSearchActivity.this, error, Toast.LENGTH_SHORT).show());
    }

    private void updateSearchState() {
        switch (getState()) {
            case Active_Search:
                finderLoadingView.setImageResource(R.drawable.progress_animation);
                break;
            case Search_Success:
                finderLoadingView.setImageResource(R.drawable.ic_check_circle_black_24dp);
                break;
            case Search_Error:
                finderLoadingView.setImageResource(R.drawable.ic_error_black_24dp);
                break;
            case Not_started:
                finderLoadingView.setImageResource(android.R.color.transparent);
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            searchAndUpdateList(false);
        }
    }


    public ProgressDialog getProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.progress_title));
        progressDialog.setMessage(getString(R.string.progress_message));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.show();
        return progressDialog;
    }

    private void updateResultText() {
        if (getState() != SearchState.Not_started) {
            if (movies.isEmpty() && getState() == SearchState.Search_Error) {
                finderResult.setText(getString(R.string.error_on_search));
            } else {
                finderResult.setText(max_search_results + " " + getString(R.string.results_found));
            }
        } else {
            finderResult.setText(getString(R.string.not_started));
        }
    }

    @OnItemClick(R.id.movie_list_view)
    void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final AtomicBoolean canceled = new AtomicBoolean(false);
        String imdbId = movie_adapter.getItem(i).getImdbID();
        Call<Movie> mc = movie_by_id.getMovie(null, imdbId, null);
        final ProgressDialog dialog = getProgressDialog();
        dialog.setOnCancelListener(di -> canceled.set(true));

        mc.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                dialog.dismiss();
                if (canceled.get()) return;
                Intent intent = new Intent(MovieSearchActivity.this, MovieInspectActivity.class);
                intent.putExtra("movie", response.body());
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                if (!canceled.get())
                    printErrorMessage(t.getMessage());
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
            searchAndUpdateList(true);
        }
    }

    public synchronized SearchState getState() {
        return state;
    }

    public synchronized void setState(SearchState state) {
        this.state = state;
    }
}
